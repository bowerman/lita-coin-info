
# lita-coin-info
[![Gem Version](https://badge.fury.io/rb/lita-coin-info.svg)](https://badge.fury.io/rb/lita-coin-info)

A Lita handler for getting cryptocurrency info

## Installation

- This is a plugin for the [Lita](http://www.lita.io) chatbot.

Add lita-coin-info to your Lita instance's Gemfile:

``` ruby
gem 'lita-coin-info'
```

See https://docs.lita.io/getting-started/plugins/ for more info.

## Usage

Works with BTC, ETH, and LTC

- `<bot_name> COIN price` -> Get current current COIN (btc, ltc, eth) spot price from coinbase.
- `<bot_name> diff` -> Get current difficulty target for BTC
