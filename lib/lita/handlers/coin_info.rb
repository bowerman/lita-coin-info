module Lita
  module Handlers
    class CoinInfo < Handler

      route(/^(btc|ltc|eth) price/, :price, command: true, help: {
        'COIN price' => 'Get current current COIN (btc, ltc, eth) spot price from coinbase.'
      })

      def price response
        coin          = response.matches[0][0]
        currency_pair = "#{ coin }-USD"
        url           = "https://api.coinbase.com/v2/prices/#{ currency_pair }/spot"

        price_json = get_and_parse_data url

        response.reply "$#{ price_json[:data][:amount] }"
      end

      route(/^(diff)/, :diff, command: true, help: {
        'diff' => 'Get current difficulty target.'
      })

      def diff response
        url = 'https://blockchain.info/q/getdifficulty'

        diff = get_data url

        response.reply diff.to_f.to_s
      end

      def get_data url
        HTTParty.get(url, format: :plain)
      end

      def get_and_parse_data url
        JSON.parse HTTParty.get(url, format: :plain), symbolize_names: true
      end

      Lita.register_handler(self)

    end
  end
end
