Gem::Specification.new do |spec|
  spec.name          = "lita-coin-info"
  spec.version       = "0.2.0"
  spec.authors       = ["Andrew Bowerman"]
  spec.email         = ["me@andrewbowerman.com"]
  spec.description   = "A Lita handler for getting cryptocurrency info"
  spec.summary       = "A Lita handler for getting cryptocurrency info"
  spec.homepage      = "https://gitlab.com/bowerman/lita-coin-info"
  spec.license       = "MIT"
  spec.metadata      = { "lita_plugin_type" => "handler" }

  spec.files         = `git ls-files`.split($/)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_runtime_dependency "lita", "~> 4.7"
  spec.add_runtime_dependency "httparty", '~> 0.15'

  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "pry-byebug"
  spec.add_development_dependency "rake"
  spec.add_development_dependency "rack-test"
  spec.add_development_dependency "rspec", ">= 3.0.0"
end
