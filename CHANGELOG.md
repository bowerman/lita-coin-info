# Change Log

## 0.2.0

- Add `<bot_name> diff` -> Get current difficulty target for BTC

## 0.1.0

- Initial Release
- `<bot_name> COIN price` -> Get current current COIN (btc, ltc, eth) spot price from coinbase.
